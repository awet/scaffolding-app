import io from 'socket.io-client';
const socket = io.connect('http://localhost:4000');

export const socketConnect = (room) => {
   console.log('room name passed is ', room);
   socket.on('connect', () => {
      console.log('connectd to server ');
   });

   socket.emit('join', { room_name : room});
   
   socket.on('disconnect', ()=> {
      console.log('disconnected from server');
   })

   socket.on('newEmail', function (obj) {
      console.log('new email arrived ', obj);
   });

   socket.on('message', function (obj) {
    console.log('new message arrived ', obj);
   });

   socket.emit('createEmail', {
       from : "earth",
       to: "to heaven"
   });
}