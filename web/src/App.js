import React, { useState, useEffect } from 'react';
import{ fetchWeather } from './api/fetchWeather';
import { socketConnect } from './api/socketConnect';
import './App.css';



const App = () => {
    useEffect(() => {
       // socketConnect();
    });

    const [query, setQuery] = useState('');
    const [weather, setWeather] = useState({});
    const [room, setRoom] = useState('');

    const search = async (e) => {
        if(e.key === 'Enter') {
            const data = await fetchWeather(query)
            setWeather(data);
            setQuery('');
            console.log(data);
        }
    }

    const roomFn = (e) => {
        if(e.key === 'Enter') {
            socketConnect(room)
            setRoom('');
        }
    }
    return (
       <div>
           <input 
               type="text"
               placeholder="search..."
               value={query}
               onChange={(e) => setQuery(e.target.value)}
               onKeyPress={search}
           />
           <input 
               type="text"
               placeholder="user-token"
               value={room}
               onChange={(e) => setRoom(e.target.value)}
               onKeyPress={roomFn}
           />
           <div>
                { weather.main ? 
                 <div> 
                   <span>{weather.name}</span> 
                   <sup>{weather.sys.country}</sup>
                   <div>{Math.round(weather.main.temp)}
                     <sup>&deg;C</sup>
                   </div>
                   <div>
                       {weather.weather[0].description}
                   </div>
                 </div> 
                 : null
                }
           </div>
       </div>
    );
}
export default App;