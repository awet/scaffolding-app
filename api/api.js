const fs = require('fs');
const http = require('http');
const express = require('express');
const app = express();
const server = http.createServer(app);
//const io = require('socket.io')(server);
const io = require('./io').init(server);
const dotenv = require('dotenv');
const hostname = '127.0.0.1';
const port = 4000;
const authRoute = require('./routes/auth');


dotenv.config();
//Middleware - api calles must be json formated 
app.use(express.json());
//Route Midleware - routes with '/api/user' forwared to authRoute
app.use('/api/user', authRoute)

app.set('socketio', io);

app.use((req, res, next)=> {
    var now = new Date().toString();
    var log = (`${now} ${req.method} ${req.url}`);
    fs.appendFile('server.log', log + '\n', (err) => {
        console.log('Unable to append to server.log.')
    });
    console.log(`${now} ${req.method} ${req.url}`);
});

io.on('connection', (socket) => {
   console.log('new user connected ', socket.id);
   socket.on('join', (room, callback) => {
       socket.join(room.room_name);
       console.log(room.room_name, ' -> ', socket.id );
   });

   io.emit('message', {rest : 'peace is jesus'});

   socket.emit('newEmail', {
        from : "Awet Tsegazeab",
        conttent : "pary and and thank to Almighty Jesus"
   });

   socket.on('createEmail', (email) => {
       console.log('New email', email);
   });

   socket.on('disconnect', (socket) => {
       console.log('client disconnected ', socket.id );
   })
})

server.listen(4000, hostname, () => {
    console.log(`server runing at http://${hostname}:${port}/`);
}); 