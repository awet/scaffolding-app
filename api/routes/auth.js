const router = require('express').Router();
const models = require('../models');
const _ = require('lodash');

router.get('/status', (req, res) => {
    var io = req.app.get('socketio');
    io.sockets.in('003994').emit("message", {rest : 'peace is jesus'});
    res.send({
        name: 'api server is up !',
        date: new Date().toString()
        
    })
});

router.get('/room', (req, res) => {
    var io = req.app.get('socketio');
    io.sockets.in('awet').emit("message", {rest : 'GOD IS GOOD'});
    res.send({
        name: 'api server is up !',
        date: new Date().toString()
        
    })
});

router.get('/test', (req, res) => {
    const io = require('../io').getio();

    io.sockets.in('awet').emit("message", {rest : 'GOD IS HOLDING MY RIGHT HAND'});
    res.send({
        name: 'api server is up !',
        date: new Date().toString()
        
    })
});


router.post('/register', async (req, res) => {
    const user = _.pick(req.body, ['first_name', 'middle_name', 'last_name', 'email', 'password', 'mobile']);
    try {
    await models.Users.create(
            user
             ).then((user) => {
                 if(user) {
                    res.send(user);
                 } else {
                     throw new Error('user could not be created');
                 }
        });
    } catch(err) {
        console.log(err);
        res.sendStatus(400).send(err);
    }
});


module.exports = router;